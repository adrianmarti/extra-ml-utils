
import warnings
import collections
from operator import attrgetter
import json
import time
import os
import subprocess
import gc

import numpy as np
import pandas as pd
import lightgbm as lgb
from sklearn import model_selection, preprocessing
from sklearn.base import clone

from lightgbm import callback
from lightgbm import Booster, Dataset
from lightgbm.compat import integer_types, range_, string_type
import mlflow


def flatten_dict(d):
    """Converts a dictionary with 2 layers of keys to one with one layer by
    concatenating key strings."""
    return {"{}_{}".format(k1, k2): d[k1][k2] for k1 in d for k2 in d[k1]}


def record_mlflow(n_fold):
    """Create a callback that records the evaluation history for mlflow."""

    def callback(env):
        """internal function"""
        for data_name, eval_name, result, _ in env.evaluation_result_list:
            mlflow.log_metric("{}_{}_fold{}".format(data_name, eval_name, n_fold), result)
    callback.order = 20
    return callback


# the lightgbm crossvalidation is not a viable solution because it doesn't have a way to have early stopping
# for all the boosters individually
def cross_validate_lgb(model, x, y, cv=None, fit_param=None, scoring='auc', mlflow=False, feature_name='auto'):
    if fit_param is None:
        fit_param = {}
    if 'verbose' not in fit_param:
        fit_param['verbose'] = 100
    if not cv:
        cv = model_selection.KFold(n_splits=5, shuffle=True)
    boosters = []
    for n_fold, (train_idx, valid_idx) in enumerate(cv.split(x, y)):
        train_x, train_y = x[train_idx], y[train_idx]
        valid_x, valid_y = x[valid_idx], y[valid_idx]

        # Using Sklearn API because it gives me a performance boost. Probably because it
        # correctly preprocesses the dataframe.
        cls = clone(model)
        if 'callbacks' not in fit_param:
            fit_param['callbacks'] = []
        if mlflow:
            fit_param['callbacks'].append(record_mlflow(n_fold))
        cls.fit(train_x, train_y, eval_set=[(train_x, train_y), (valid_x, valid_y)], eval_names=['train', 'valid'], eval_metric=scoring,
                                        feature_name=feature_name, **fit_param)

        del train_x, train_y, valid_x, valid_y
        gc.collect()
        boosters.append(cls.booster_)

    return boosters


def get_last_commit():
    # passing subprocess.PIPE makes run capture stdout, encoding is passed so it
    # doesn't give a byte array that can't be serialized later
    completed_process = subprocess.run(["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE, encoding='utf-8')
    return completed_process.stdout[:-1]


def get_best_iterations(boosters):
    flat_dicts = [flatten_dict(booster.best_score) for booster in boosters]
    by_key = {key: [flat_dict[key] for flat_dict in flat_dicts] for key in flat_dicts[0]}
    means = {"{}_mean".format(key): np.mean(by_key[key]) for key in by_key}
    stds = {"{}_std".format(key): np.std(by_key[key]) for key in by_key}
    means.update(stds)
    return means


# sklearn style cross_validate, with special care to lightgbm models 
def cross_validate_mlflow(model, x, y, cv=None, fit_param=None, label=None, log=True, feature_name='auto'):
    x = np.array(x)
    y = np.array(y)
    if not cv:
        cv = model_selection.KFold(n_splits=5, shuffle=True)
    
    def _inner(x):
        cv_start_time = time.time()
        if isinstance(model, lgb.LGBMModel):
            models = cross_validate_lgb(model, x, y, cv=cv, fit_param=fit_param, scoring='auc', feature_name=feature_name)

            if log:
                best_iteration_metrics = get_best_iterations(models)
                for metric_key in best_iteration_metrics:
                    mlflow.log_metric(metric_key, best_iteration_metrics[metric_key])

                path = gen_model_path(label)
                os.mkdir(path)
                save_models_cv(models, label)

        else:
            scaler = preprocessing.StandardScaler()
            x = scaler.fit_transform(x)
            results = model_selection.cross_validate(model, x, y, cv=cv, scoring='roc_auc',
                                                            return_train_score=True, return_estimator=True)
            models = list(results['estimator'])

            print('train_scores:', results['train_score'])
            print('test_scores:', results['test_score'])
            if log:
                mlflow.log_metric("train_score_mean", np.mean(results['train_score']))
                mlflow.log_metric("train_score_std", np.std(results['train_score']))

                mlflow.log_metric("test_score_mean", np.mean(results['test_score']))
                mlflow.log_metric("test_score_std", np.std(results['test_score']))

        if log:
            mlflow.log_param('commit_hash', get_last_commit())
            cv_time = time.time() - cv_start_time
            mlflow.log_metric('time', cv_time)
            
            if cv.random_state:
                mlflow.log_param('random_state', random_state)
        
        return models
    
    if log:
        with mlflow.start_run(run_name=label):
            models = _inner(x)
    else:
        models = _inner(x)
        
    return models


def save_models_cv(boosters, label):
    for i, booster in enumerate(boosters):
        booster.save_model('../models/{}/model_fold_{}'.format(label, i))


def gen_model_path(label):
    path = '../models/{}/'.format(label)
    i = 1
    while os.path.exists(path):
        i += 1
        path = '../models/{}_{}/'.format(label, i)
    
    return path


# Now some utility functions for results analysis
def load_models_cv(label):
    model_path = '../models/{}/'.format(label)
    fold_paths = [(int(filename[-1]), model_path + filename) for filename in os.listdir(model_path) if filename[:-1] == 'model_fold_']

    # The list should have the boosters in the order as the original list.
    # Note that .sort() on tuples defines the order as the order on the first element
    fold_paths.sort()
    return [lgb.Booster(model_file=path) for _, path in fold_paths]


def get_feature_importances_cv(boosters):
    names = boosters[0].feature_name()
    split_importances = [("fold_{}_split".format(i), booster.feature_importance(importance_type='split')) for i, booster in enumerate(boosters)]
    gain_importances = [("fold_{}_gain".format(i), booster.feature_importance(importance_type='gain')) for i, booster in enumerate(boosters)]

    return pd.DataFrame(dict(split_importances + gain_importances), names)


def make_submission(label, features='selected_features.h5', target='target'):
    boosters = load_models_cv(label)
    ids = pd.read_hdf("../data/interm/binary_data.h5", key='application_test').SK_ID_CURR
    features = pd.read_hdf('../data/processed/' + features)
    features = features[features.TARGET.isnull()].drop(target, axis=1)
    dataset = lgb.Dataset(data=features)

    sub_preds = np.zeros(len(ids))  # for some reason lgbm predicts negative values up to -0.068, but kaggle doesn't accept that
    for booster in boosters:
        sub_preds = sub_preds + booster.predict(features) / len(boosters)

    submission = pd.DataFrame({'SK_ID_CURR': ids, target: sub_preds})
    submission.to_csv("../submissions/{}.csv".format(label), index=False)
